import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BlogsModule } from './blogs/blogs.module';
import { UsersModule } from './users/users.module';
import { ReviewsModule } from './reviews/reviews.module';
import { CommentsModule } from './comments/comments.module';
import { AuthModule } from './auth/auth.module';
import { SaveModule } from './save/save.module';
import { LikeModule } from './like/like.module';
import { HistoryModule } from './history/history.module';
import { ReactModule } from './react/react.module';
import { SearchModule } from './search/search.module';
import { ImagesModule } from './images/images.module';
import { FacebookModule } from './facebook/facebook.module';
import { GoogleModule } from './google/google.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRootAsync({
      useFactory: () => ({ uri: process.env.MONGODB_URI }),
    }),
    GraphQLModule.forRoot({
      playground: true,
      autoSchemaFile: true,
    }),
    BlogsModule,
    UsersModule,
    ReviewsModule,
    CommentsModule,
    AuthModule,
    SaveModule,
    LikeModule,
    HistoryModule,
    ReactModule,
    SearchModule,
    ImagesModule,
    FacebookModule,
    GoogleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

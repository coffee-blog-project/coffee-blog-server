import { Injectable, ExecutionContext, CanActivate } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {
  constructor() {
    super({ property: 'user' });
  }

  async canActivate(context: ExecutionContext) {
    await super.canActivate(context);
    const ctx = GqlExecutionContext.create(context);
    const request = ctx.getContext();
    request.body = ctx.getArgs();
    return true;
  }

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    const request = ctx.getContext();
    request.body = ctx.getArgs();
    return request;
  }
}

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor() {
    super({ property: 'user' });
  }

  async canActivate(context: ExecutionContext) {
    await super.canActivate(context);
    const ctx = GqlExecutionContext.create(context);
    const request = ctx.getContext();
    request.user = request.req.user;
    return true;
  }

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    const request = ctx.getContext();
    return request.req;
  }
}

@Injectable()
export class WeakJwtAuthGuard extends JwtAuthGuard {
  async canActivate(context: ExecutionContext) {
    try {
      await super.canActivate(context);
    } catch (e) {
      if (e.status !== 401) throw e;
    }
    return true;
  }
}

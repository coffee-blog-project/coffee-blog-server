import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.model';
import { compareSync } from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string) {
    const user = await this.usersService.getUserByEmail(email);
    if (user && compareSync(password, user.password)) {
      return user;
    }
    return null;
  }

  async generateJwt({ _id, name, email }: User) {
    const payload = { _id, name, email };
    return this.jwtService.sign(payload);
  }
}

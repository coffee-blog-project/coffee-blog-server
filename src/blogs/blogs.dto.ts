import { InputType, Field, PartialType } from '@nestjs/graphql';
import { ContentFormatType, CategoryType } from './blogs.model';

export interface GetBlogArgs {
  writerId?: string;
  published?: boolean;
}

export type GetBlogsArgs = GetBlogArgs & {
  category?: CategoryType
}

export type SearchBlogsArgs = GetBlogsArgs

export interface CreateBlogArgs {
  writerId: string;
  title: string;
  imageHeaderUrl?: string;
  contentFormat: ContentFormatType;
  content: string;
  published: boolean;
}

@InputType('CreateBlogArgs')
export class CreateBlogArgsInputType
  implements Omit<CreateBlogArgs, 'writerId'> {
  @Field(() => String)
  title: string;

  @Field(() => String, { nullable: true })
  imageHeaderUrl?: string;

  @Field(() => ContentFormatType)
  contentFormat: ContentFormatType;

  @Field(() => String)
  content: string;

  @Field(() => Boolean, { nullable: true })
  published: boolean;

  @Field(() => CategoryType)
  category: CategoryType
}

export type UpdateBlogArgs = Partial<Omit<CreateBlogArgs, 'writerId'>>;

@InputType('UpdateBlogArgs')
export class UpdateBlogArgsInputType extends PartialType(
  CreateBlogArgsInputType,
) {}

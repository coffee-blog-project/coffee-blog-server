import {
  ObjectType,
  Field,
  registerEnumType,
  GraphQLISODateTime,
  ID,
} from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export enum ContentFormatType {
  html = 'html',
  markdown = 'markdown',
  editorjs = 'editorjs',
}
registerEnumType(ContentFormatType, { name: 'ContentFormatType' });

export enum CategoryType {
  process = 'process',
  health = 'health',
  sustainability = 'sustainability',
  industry = 'industry',
  experience = 'experience',
  other = 'other',
}
registerEnumType(CategoryType, { name: 'CategoryType' });

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@ObjectType()
export class Blog extends Document {
  @Field(() => ID)
  _id: Types.ObjectId;

  @Field(() => GraphQLISODateTime)
  _createdAt: Date;

  @Field(() => GraphQLISODateTime)
  _updatedAt: Date;

  @Field(() => String)
  @Prop({ type: String })
  writerId: string;

  @Field(() => String)
  @Prop({ required: true })
  title: string;

  @Field(() => String, { nullable: true })
  @Prop()
  imageHeaderUrl?: string;

  @Field(() => ContentFormatType)
  @Prop({ required: true, enum: Object.values(ContentFormatType) })
  contentFormat: ContentFormatType;

  @Field(() => String)
  @Prop({ required: true })
  content: string;

  @Field(() => Boolean, { defaultValue: false })
  @Prop({ default: false })
  published: boolean

  @Field(() => CategoryType)
  @Prop({ required: true })
  category: CategoryType
}

export const BlogSchema = SchemaFactory
  .createForClass(Blog)
  .index({ title: 'text', content: 'text', writerId: 'text' })

import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BLOGS_SCHEMA_TOKEN } from './blogs.constant';
import { BlogSchema } from './blogs.model';
import { BlogsService } from './blogs.service';
import { BlogsResolver } from './blogs.resolver';
import { UsersModule } from '../users/users.module';
import { CommentsModule } from '../comments/comments.module';
import { LikeModule } from '../like/like.module';
import { HistoryModule } from '../history/history.module';
import { SaveModule } from '../save/save.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: BLOGS_SCHEMA_TOKEN,
        schema: BlogSchema,
      },
    ]),
    forwardRef(() => UsersModule),
    CommentsModule,
    LikeModule,
    HistoryModule,
    SaveModule,
  ],
  providers: [BlogsService, BlogsResolver],
  exports: [BlogsService],
})
export class BlogsModule {}

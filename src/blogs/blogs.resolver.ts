import {
  Resolver,
  Mutation,
  Query,
  Args,
  ResolveField,
  Parent,
  Context,
  Int,
} from '@nestjs/graphql';
import { UnauthorizedException, UseGuards } from '@nestjs/common';
import { BlogsService } from './blogs.service';
import { Blog, CategoryType } from './blogs.model';
import { CreateBlogArgsInputType, UpdateBlogArgsInputType } from './blogs.dto';
import { User } from '../users/users.model';
import { UsersService } from '../users/users.service';
import { CommentsService } from '../comments/comments.service';
import { Comment } from '../comments/comments.model';
import { AddressType } from '../react/react.model';
import { JwtAuthGuard, WeakJwtAuthGuard } from '../auth/auth.guard';
import { LikeService } from '../like/like.service';
import { HistoryService } from '../history/history.service';
import { Ip } from '../libs/decorators/ip';
import { SaveService } from '../save/save.service';

@Resolver(() => Blog)
export class BlogsResolver {
  constructor(
    private readonly blogsService: BlogsService,
    private readonly usersService: UsersService,
    private readonly commentsService: CommentsService,
    private readonly likeService: LikeService,
    private readonly historyService: HistoryService,
    private readonly saveService: SaveService,
  ) {}

  @UseGuards(WeakJwtAuthGuard)
  @Query(() => Blog, { nullable: true })
  async blog(
    @Args({
      name: '_id',
      type: () => String,
    })
    _id: string,
    @Context('user') user: User,
    @Ip() ip: string,
  ) {
    const blog = await this.blogsService.getBlog(
      _id,
      user && {
        writerId: user._id as string,
      },
    );

    if (blog.published) {
      this.historyService.createHistory({
        to: {
          type: AddressType.blog,
          _id,
        },
        from: {
          userId: user && (user._id as string),
          ip,
        },
      });
    }

    return blog;
  }

  @UseGuards(WeakJwtAuthGuard)
  @Query(() => [Blog])
  blogs(
    @Args({
      name: 'category',
      type: () => CategoryType,
      nullable: true,
    })
    category: CategoryType
  ) {
    return this.blogsService.getBlogs({
      published: true,
      category,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Blog)
  createBlog(
    @Args({
      name: 'blog',
      type: () => CreateBlogArgsInputType,
    })
    blog: CreateBlogArgsInputType,
    @Context('user') { _id }: User,
  ) {
    return this.blogsService.createBlog({
      ...blog,
      writerId: _id as string,
    });
  }

  @Mutation(() => Blog)
  @UseGuards(JwtAuthGuard)
  async updateBlog(
    @Args({
      name: '_id',
      type: () => String,
    })
    _id: string,
    @Args({
      name: 'blog',
      type: () => UpdateBlogArgsInputType,
    })
    blogArgs: UpdateBlogArgsInputType,
    @Context('user') { _id: writerId }: User,
  ) {
    const blog = await this.blogsService.updateBlog(
      _id,
      writerId as string,
      blogArgs,
    );
    if (!blog) throw new UnauthorizedException();
    return blog;
  }

  @ResolveField(() => User)
  writer(@Parent() { writerId }: Blog) {
    return this.usersService.getUserById(writerId);
  }

  @ResolveField(() => [Comment])
  comments(@Parent() { _id }: Blog) {
    return this.commentsService.getComments({
      type: AddressType.blog,
      _id: _id.toHexString(),
    });
  }

  @ResolveField(() => Boolean, { nullable: true })
  async liked(@Parent() { _id }: Blog, @Context('user') user?: User) {
    const res = user
      ? !!(await this.likeService.getLike({
          to: {
            type: AddressType.blog,
            _id: _id.toHexString(),
          },
          from: {
            userId: user._id as string,
          },
        }))
      : null;
    return res;
  }

  @ResolveField(() => Int, { defaultValue: 0 })
  async likes(@Parent() { _id }: Blog) {
    return this.likeService.getLikesCount({
      type: AddressType.blog,
      _id: _id.toHexString(),
    });
  }

  @ResolveField(() => Int, { defaultValue: 0 })
  async views(@Parent() { _id }: Blog) {
    return this.historyService.getViewsCount({
      type: AddressType.blog,
      _id: _id.toHexString(),
    });
  }

  @ResolveField(() => Boolean, { nullable: true })
  async saved(@Parent() { _id }: Blog, @Context('user') user?: User) {
    return (
      user &&
      !!(await this.saveService.getSingleSave({
        to: {
          type: AddressType.blog,
          _id: _id.toHexString(),
        },
        from: {
          userId: user._id as string,
        },
      }))
    );
  }
}

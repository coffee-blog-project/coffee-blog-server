import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BlogsService } from './blogs.service';
import { Blog, BlogSchema } from './blogs.model';
import { BLOGS_SCHEMA_TOKEN } from './blogs.constant';

describe('BlogsService', () => {
  let service: BlogsService;
  let mongoose: Model<Blog>;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({ envFilePath: '.env.test' }),
        MongooseModule.forRootAsync({
          useFactory: () => ({ uri: process.env.MONGODB_URI }),
        }),
        MongooseModule.forFeature([
          { name: BLOGS_SCHEMA_TOKEN, schema: BlogSchema },
        ]),
      ],
      providers: [BlogsService],
    }).compile();

    service = module.get<BlogsService>(BlogsService);
    mongoose = module.get<Model<Blog>>(getModelToken(BLOGS_SCHEMA_TOKEN));

    await mongoose.deleteMany({});
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(mongoose).toBeDefined();
  });
});

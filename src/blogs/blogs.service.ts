import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { BLOGS_SCHEMA_TOKEN } from './blogs.constant';
import { Blog } from './blogs.model';
import { GetBlogsArgs, GetBlogArgs, CreateBlogArgs, UpdateBlogArgs, SearchBlogsArgs } from './blogs.dto';
import * as _ from 'lodash'

@Injectable()
export class BlogsService implements OnModuleInit {
  constructor(
    @InjectModel(BLOGS_SCHEMA_TOKEN)
    private readonly blogModel: Model<Blog>,
  ) {}

  async onModuleInit() {
    await this.blogModel.syncIndexes()
  }

  getBlogs(args?: GetBlogsArgs) {
    args = _.pickBy(args, _.identity)    
    return this.blogModel.find(args).sort([['_createdAt', -1]]);
  }

  getBlog(_id: Types.ObjectId | string, args?: GetBlogArgs) {
    return this.blogModel.findOne({
      _id,
      ...(args && {
        $or: [{ writerId: args.writerId }, { published: true }],
      }),
    });
  }

  createBlog(args: CreateBlogArgs) {
    return this.blogModel.create(args);
  }

  updateBlog(
    _id: Types.ObjectId | string,
    writerId: string,
    args: UpdateBlogArgs,
  ) {
    return this.blogModel.findOneAndUpdate({ _id, writerId }, args).exec();
  }

  search(query: string, args?: SearchBlogsArgs) {
    args = _.pickBy(args, _.identity)    
    return this.blogModel.find({ 
      ...args,
      $text: { $search: query 
    }}).exec()
  }
}

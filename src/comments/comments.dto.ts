import { InputType, Field } from '@nestjs/graphql';
import { AddressType } from '../react/react.model';

@InputType()
export class CommentToArgs {
  @Field(() => AddressType)
  type: AddressType;

  @Field(() => String)
  _id: string;
}

export class CommentFromArgs {
  userId: string;
}

@InputType()
export class CreateCommentArgs {
  @Field(() => CommentToArgs)
  to: CommentToArgs;

  from: CommentFromArgs;

  @Field(() => String)
  content: string;
}

import { ObjectType, Field, registerEnumType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { React, ReactTo, ReactFrom } from '../react/react.model';

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@ObjectType()
export class Comment extends React {
  @Prop(() => String)
  @Field(() => String)
  content: string;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);

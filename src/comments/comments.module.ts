import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CommentsService } from './comments.service';
import { CommentsResolver, CommentFromResolver } from './comments.resolver';
import { COMMENTS_SCHEMA_TOKEN } from './comments.constant';
import { CommentSchema } from './comments.model';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: COMMENTS_SCHEMA_TOKEN,
        schema: CommentSchema,
      },
    ]),
    forwardRef(() => UsersModule),
  ],
  providers: [CommentsService, CommentsResolver, CommentFromResolver],
  exports: [CommentsService],
})
export class CommentsModule {}

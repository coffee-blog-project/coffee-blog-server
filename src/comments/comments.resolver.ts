import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
  Context,
} from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { CreateCommentArgs } from './comments.dto';
import { Comment } from './comments.model';
import { CommentsService } from './comments.service';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.model';
import { JwtAuthGuard } from '../auth/auth.guard';
import { ReactFrom } from '../react/react.model';

@Resolver(() => ReactFrom)
export class CommentFromResolver {
  constructor(private readonly usersService: UsersService) {}

  @ResolveField(() => User)
  user(@Parent() { userId }: ReactFrom) {
    return this.usersService.getUserById(userId);
  }
}

@Resolver(() => Comment)
export class CommentsResolver {
  constructor(private readonly commentsService: CommentsService) {}

  @Query(() => [Comment])
  comments() {
    return this.commentsService.getComments();
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Comment)
  createComment(
    @Args({
      name: 'comment',
      type: () => CreateCommentArgs,
    })
    comment: CreateCommentArgs,
    @Context('user') { _id }: User,
  ) {
    return this.commentsService.createComment({
      ...comment,
      from: {
        userId: _id as string,
      },
    });
  }
}

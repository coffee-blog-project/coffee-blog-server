import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Comment } from './comments.model';
import { COMMENTS_SCHEMA_TOKEN } from './comments.constant';
import { CreateCommentArgs, CommentToArgs } from './comments.dto';

@Injectable()
export class CommentsService {
  constructor(
    @InjectModel(COMMENTS_SCHEMA_TOKEN)
    private readonly commentModel: Model<Comment>,
  ) {}

  getComments(to?: CommentToArgs) {
    const stmt = this.commentModel.find();
    if (to) stmt.where('to').equals(to);
    return stmt;
  }

  createComment(args: CreateCommentArgs) {
    return this.commentModel.create(args);
  }
}

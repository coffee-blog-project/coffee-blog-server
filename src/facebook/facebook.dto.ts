export interface FacebookOAuthResponse {
  access_token: string
  token_type: string
  expires_in: number
}

export interface FacebookUser {
  id: string
  name: string
}

import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { FacebookService } from './facebook.service';
import { FacebookResolver } from './facebook.resolver';
import { AuthModule } from '../auth/auth.module'
import { UsersModule } from '../users/users.module'

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        baseURL: 'https://graph.facebook.com/v10.0/',
      }),
    }),
    forwardRef(() => AuthModule),
    forwardRef(() => UsersModule),
  ],
  providers: [FacebookService, FacebookResolver]
})
export class FacebookModule {}

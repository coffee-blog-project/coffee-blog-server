import { Test, TestingModule } from '@nestjs/testing';
import { FacebookResolver } from './facebook.resolver';

describe('FacebookResolver', () => {
  let resolver: FacebookResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FacebookResolver],
    }).compile();

    resolver = module.get<FacebookResolver>(FacebookResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});

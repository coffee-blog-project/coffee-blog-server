import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { FacebookService } from './facebook.service'
import { UsersService } from '../users/users.service'
import { AuthService } from '../auth/auth.service';

@Resolver()
export class FacebookResolver {
  constructor(
    private readonly authService: AuthService,
    private readonly facebookService: FacebookService,
    private readonly usersService: UsersService,
  ) {}

  @Mutation(() => String)
  async resolveFacebookOAuth(
    @Args({
      name: 'code',
      type: () => String
    }) 
    code: string,
    @Args({
      name: 'redirectUri',
      type: () => String
    }) 
    redirectUri: string,
  ) {
    const { access_token } = await this.facebookService
      .getAccessTokenWithOAuthCode(code, redirectUri)

    const facebookUser = await this.facebookService.getUserInfo(access_token)

    let user = await this.usersService.getUserByFacebookId(facebookUser.id)

    if (!user) {
      user = await this.usersService.createUser({
        facebookId: facebookUser.id,
        name: facebookUser.name,
      }) 
    }
    
    return this.authService.generateJwt(user)
  }
}

import { Injectable, HttpService } from '@nestjs/common';
import { FacebookOAuthResponse, FacebookUser } from './facebook.dto'

@Injectable()
export class FacebookService {
  constructor(private readonly httpService: HttpService) {}

  async getAccessTokenWithOAuthCode(code: string, redirectUri: string): Promise<FacebookOAuthResponse> {
    const { data } = await this.httpService.get('/oauth/access_token', {
      params: {
        'client_id': process.env.FACEBOOK_CLIENT_ID,
        'client_secret': process.env.FACEBOOK_SECRET,
        'redirect_uri': redirectUri,
        code,
      }
    }).toPromise()

    return data
  }

  async getUserInfo(accessToken: string): Promise<FacebookUser> {
    const { data } = await this.httpService.get('/me', {
      params: {
        'access_token': accessToken,
      }
    }).toPromise()

    return data
  }
}

export interface GoogleOAuthResponse {
  access_token: string
  expires_in: number
  refresh_token: string
  scope: string
  token_type: string
}

export interface GoogleUser {
  id: string
  name: string
  email: string
}
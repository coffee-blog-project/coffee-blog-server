import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { GoogleService } from './google.service'
import { UsersService } from '../users/users.service'
import { AuthService } from '../auth/auth.service';

@Resolver()
export class GoogleResolver {
  constructor(
    private readonly googleService: GoogleService,
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Mutation(() => String)
  async resolveGoogleOAuth(
    @Args({
      name: 'code',
      type: () => String
    }) 
    code: string,
    @Args({
      name: 'redirectUri',
      type: () => String
    }) 
    redirectUri: string,
  ) {
    const { access_token } = await this.googleService
      .getAccessTokenWithOAuthCode(code, redirectUri)

    const googleUser = await this.googleService.getUserInfo(access_token)
    
    let user = await this.usersService.getUserByGoogleId(googleUser.id)

    if (!user) {
      user = await this.usersService.createUser({
        googleId: googleUser.id,
        name: googleUser.name,
      }) 
    }
    
    return this.authService.generateJwt(user)
  }
}

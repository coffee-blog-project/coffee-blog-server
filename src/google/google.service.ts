import { Injectable, HttpService } from '@nestjs/common';
import { GoogleOAuthResponse, GoogleUser } from './google.dto'

@Injectable()
export class GoogleService {
  constructor(private readonly httpService: HttpService) {}

  async getAccessTokenWithOAuthCode(code: string, redirectUri: string): Promise<GoogleOAuthResponse> {
    const { data } = await this.httpService.post('https://oauth2.googleapis.com/token', {
      'client_id': process.env.GOOGLE_CLIENT_ID,
      'client_secret': process.env.GOOGLE_SECRET,
      'redirect_uri': redirectUri,
      'grant_type': 'authorization_code',
      code,
    }).toPromise()

    return data
  }

  async getUserInfo(accessToken: string): Promise<GoogleUser> {
    const { data } = await this.httpService.get('https://www.googleapis.com/oauth2/v1/userinfo', {
      params: {
        'access_token': accessToken,
      }
    }).toPromise()

    return data
  }
}

import { ReactTo, ReactFrom } from '../react/react.model';

export interface CreateHistoryArgs {
  to: ReactTo;
  from?: Partial<ReactFrom> & {
    ip: string;
  };
}

export interface AggregateViewCount {
  viewCount: number;
}

export interface AggregateTopWeekly {
  to: ReactTo
  week: number
  count: number
}

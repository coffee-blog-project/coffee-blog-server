import { ObjectType, Field } from '@nestjs/graphql'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ReactFrom, ReactToSchema, ReactTo } from '../react/react.model';

@Schema({
  _id: false,
})
export class HistoryFrom implements Partial<ReactFrom> {
  @Prop({ type: String })
  userId?: string;

  @Prop({ type: String, default: null })
  ip: string;
}

const HistroyFromSchema = SchemaFactory.createForClass(HistoryFrom);

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@ObjectType()
export class History extends Document {
  @Prop({ type: ReactToSchema, required: true })
  @Field(() => ReactTo)
  to: ReactTo;

  @Prop({ type: HistroyFromSchema, required: false })
  @Field(() => ReactFrom)
  from?: ReactFrom;
}

export const HistorySchema = SchemaFactory.createForClass(History);

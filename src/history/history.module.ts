import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HistoryService } from './history.service';
import { HistoryResolver } from './history.resolver';
import { HistorySchema } from './history.model';
import { HISTORY_SCHEMA_TOKEN } from './histroy.constant';
import { BlogsModule } from '../blogs/blogs.module';
import { ReviewsModule } from '../reviews/reviews.module'

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: HISTORY_SCHEMA_TOKEN,
        schema: HistorySchema,
      },
    ]),
    forwardRef(() => BlogsModule),
    forwardRef(() => ReviewsModule),
  ],
  providers: [HistoryService, HistoryResolver],
  exports: [HistoryService],
})
export class HistoryModule {}

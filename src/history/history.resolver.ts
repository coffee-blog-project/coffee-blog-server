import { Resolver, Query, ResolveField, Parent } from '@nestjs/graphql';
import { HistoryService } from '../history/history.service'
import { History } from '../history/history.model'
import { BlogsService } from '../blogs/blogs.service';
import { ReviewsService } from '../reviews/reviews.service'
import { Post } from '../libs/models/post'
import { AddressType } from '../react/react.model';

@Resolver(() => History)
export class HistoryResolver {
  constructor(
    private readonly blogsService: BlogsService,
    private readonly reviewsService: ReviewsService,
    private readonly historyResolver: HistoryService,
  ) {}

  @Query(() => [History])
  async topWeekly() {
    return this.historyResolver.topWeekly(5)
  }

  @ResolveField(() => Post)
  source(@Parent() history: History) {
    if (history.to.type === AddressType.blog)
      return this.blogsService.getBlog(history.to._id);
    if (history.to.type === AddressType.review)
      return this.reviewsService.getReview(history.to._id);
  }
}

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { History } from './history.model';
import { CreateHistoryArgs, AggregateViewCount, AggregateTopWeekly } from './history.dto';
import { ReactTo } from '../react/react.model';
import { HISTORY_SCHEMA_TOKEN } from './histroy.constant';

@Injectable()
export class HistoryService {
  constructor(
    @InjectModel(HISTORY_SCHEMA_TOKEN)
    private readonly historyModel: Model<History>,
  ) {}

  createHistory(args: CreateHistoryArgs) {
    return this.historyModel.create(args);
  }

  async getViewsCount(to: ReactTo) {
    const res = await this.historyModel
      .aggregate<AggregateViewCount>()
      .match({ to })
      .project({
        from: '$from',
        date: {
          $dateToString: {
            format: '%Y-%m-%d',
            date: '$_createdAt',
          },
        },
      })
      .group({
        _id: {
          from: '$from',
          date: '$date',
        },
      })
      .count('viewCount');

    return res[0]?.viewCount || 0
  }

  async topWeekly(n: number) {
    const res = await this.historyModel
      .aggregate<AggregateTopWeekly>()
      .project({
        to: '$to',
        from: '$from',
        week: {
          $week: {
            date: '$_createdAt',
          }
        },
        date: {
          $dateToString: {
            format: '%Y-%m-%d',
            date: '$_createdAt',
          },
        },
      })
      .group({
        _id: {
          to: '$to',
          from: '$from',
          date: '$date',
        },
        to: {
          $first: '$to',
        },
        week: {
          $first: '$week',
        },
      })
      .group({
        _id: {
          to: '$to',
          week: '$week',
        },
        to: {
          $first: '$to',
        },
        week: {
          $first: '$week',
        },
        count: {
          $sum: 1,
        },
      })
      .sort({
        week: -1,
        count: -1,
      })
      .limit(n)

    return res
  }
}

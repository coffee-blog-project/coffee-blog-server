import { Controller, Post, UseInterceptors, UploadedFile, Param, Res, Get } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express'
import { ImagesService } from './images.service'
import { Express, Response } from 'express'

@Controller('images')
export class ImagesController {
  constructor(private readonly imagesService: ImagesService) {}

  @UseInterceptors(FileInterceptor('file'))
  @Post()
  async uploadImage(@UploadedFile() file: Express.Multer.File) {
    const url = await this.imagesService.uploadImage(file.buffer)
    return {
      url
    }
  }

  @Get(':fileName')
  async getImage(
    @Param('fileName') fileName: string,
    @Res() response: Response
  ) {
    const { file, mime } = await this.imagesService.getImage(fileName)
    response.contentType(mime)
    response.send(file)
  }
}

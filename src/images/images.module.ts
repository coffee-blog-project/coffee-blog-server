import { Module } from '@nestjs/common';
import { ImagesService } from './images.service';
import { ImagesController } from './images.controller';
import { S3Module } from 'nestjs-s3'

@Module({
  imports: [
    S3Module.forRootAsync({
      useFactory: () => ({
        config: {
          accessKeyId: process.env.MINIO_ACCESS_KEY,
          secretAccessKey: process.env.MINIO_SECRET_KEY,
          endpoint: process.env.MINIO_ENDPOINT,
          sslEnabled: process.env.MINIO_USE_SSL === 'true',
          s3ForcePathStyle: true,
          signatureVersion: 'v4',
        }
      })
    }),
  ],
  providers: [ImagesService],
  controllers: [ImagesController]
})
export class ImagesModule {}

import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectS3, S3 } from 'nestjs-s3'
import { ObjectId } from 'mongodb'
import imageType from 'image-type'

@Injectable()
export class ImagesService implements OnModuleInit {
  protected bucketName: string

  constructor(
    @InjectS3() private readonly s3: S3,
  ) {
    this.bucketName = 'coffee-blog'
  }

  async onModuleInit() {
    try {
      await this.s3.createBucket({ Bucket: this.bucketName }).promise()
    } catch(e) {}
  }

  async uploadImage(image: Buffer) {
    const id = new ObjectId()
    const { ext } = imageType(image)
    const fileName = `${id.toHexString()}.${ext}`

    // await this.minioClient.putObject(this.bucketName, fileName, image)
    await this.s3.putObject({ 
      Bucket: this.bucketName, 
      Key: fileName,
      Body: image,
    }).promise()

    const url = new URL(`images/${fileName}`, process.env.APP_URL).href
    return url
  }

  async getImage(name: string) {
    // const file = await this.minioClient.getObject(this.bucketName, name)
    // return file.read()
    const response = await this.s3.getObject({
      Bucket: this.bucketName,
      Key: name,
    }).promise()

    const image = response.Body as Buffer
    const type = imageType(image)

    return {
      ...type,
      file: image,
    }
  }
}

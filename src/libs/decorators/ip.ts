import * as requestIp from 'request-ip';
import {
  createParamDecorator,
  ExecutionContext,
  Request,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const Ip = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();
    return requestIp.getClientIp(req);
  },
);

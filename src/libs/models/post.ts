import { createUnionType } from '@nestjs/graphql'
import { Blog } from '../../blogs/blogs.model'
import { Review } from '../../reviews/reviews.model'

export const Post = createUnionType({
  name: 'Post',
  types: () => [Blog, Review],
  resolveType(value) {
    if (value.productName) {
      return Review
    }
    if (value.title) {
      return Blog
    }
    return null
  }
})
import { InputType, Field } from '@nestjs/graphql';
import { AddressType } from '../react/react.model';

export interface GetLikeArgs {
  to: LikeToArgs;
  from: LikeFromArgs;
}

@InputType()
export class LikeToArgs {
  @Field(() => AddressType)
  type: AddressType;

  @Field(() => String)
  _id: string;
}

export class LikeFromArgs {
  userId: string;
}

@InputType()
export class CreateLikeArgs {
  @Field(() => LikeToArgs)
  to: LikeToArgs;

  from: LikeFromArgs;
}

@InputType()
export class DeleteLikeArgs extends CreateLikeArgs {}

import { ObjectType } from '@nestjs/graphql';
import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { React, ReactTo, ReactFrom } from '../react/react.model';

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@ObjectType()
export class Like extends React {}

export const LikeSchema = SchemaFactory.createForClass(Like).index(
  { to: 1, from: 1 },
  { unique: true },
);

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LikeService } from './like.service';
import { LikeResolver } from './like.resolver';
import { LikeSchema } from './like.model';
import { LIKE_SCHEMA_TOKEN } from './like.constant';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: LIKE_SCHEMA_TOKEN, schema: LikeSchema },
    ]),
  ],
  providers: [LikeService, LikeResolver],
  exports: [LikeService],
})
export class LikeModule {}

import { Resolver, Mutation, Args, Context } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/auth.guard';
import { LikeService } from './like.service';
import { Like } from './like.model';
import { CreateLikeArgs, DeleteLikeArgs } from './like.dto';
import { User } from '../users/users.model';

@Resolver()
export class LikeResolver {
  constructor(private readonly likeService: LikeService) {}

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Like)
  createLike(
    @Args({
      name: 'like',
      type: () => CreateLikeArgs,
    })
    like: CreateLikeArgs,
    @Context('user') { _id }: User,
  ) {
    return this.likeService.createLike({
      ...like,
      from: {
        userId: _id as string,
      },
    });
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Boolean)
  async deleteLike(
    @Args({
      name: 'like',
      type: () => DeleteLikeArgs,
    })
    like: DeleteLikeArgs,
    @Context('user') { _id }: User,
  ) {
    const res = await this.likeService.deleteLike({
      ...like,
      from: {
        userId: _id as string,
      },
    });
    return res.n === 1;
  }
}

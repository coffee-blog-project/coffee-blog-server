import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { LIKE_SCHEMA_TOKEN } from './like.constant';
import { Like } from './like.model';
import {
  CreateLikeArgs,
  DeleteLikeArgs,
  GetLikeArgs,
  LikeToArgs,
} from './like.dto';

@Injectable()
export class LikeService implements OnModuleInit {
  constructor(
    @InjectModel(LIKE_SCHEMA_TOKEN)
    private readonly likeModel: Model<Like>,
  ) {}

  async onModuleInit() {
    await this.likeModel.syncIndexes();
  }

  async createLike(args: CreateLikeArgs) {
    return this.likeModel.create(args);
  }

  async deleteLike(args: DeleteLikeArgs) {
    return this.likeModel.deleteOne(args).exec();
  }

  async getLike(args: GetLikeArgs) {
    return this.likeModel
      .findOne({
        ...args,
      })
      .exec();
  }

  async getLikesCount(to: LikeToArgs) {
    return this.likeModel
      .find({
        to,
      })
      .count()
      .exec();
  }
}

import {
  ObjectType,
  InterfaceType,
  Field,
  registerEnumType,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export enum AddressType {
  review = 'review',
  blog = 'blog',
}
registerEnumType(AddressType, { name: 'AddressType' });

@Schema({
  _id: false,
})
@ObjectType()
export class ReactTo {
  @Prop({ type: String, enum: AddressType })
  @Field(() => AddressType)
  type: AddressType;

  @Prop({ type: String })
  @Field(() => String)
  _id: string;
}
export const ReactToSchema = SchemaFactory.createForClass(ReactTo);

@Schema({
  _id: false,
})
@ObjectType()
export class ReactFrom {
  @Prop({ type: String })
  @Field(() => String)
  userId: string;
}
export const ReactFromSchema = SchemaFactory.createForClass(ReactFrom);

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@InterfaceType()
export class React extends Document {
  @Field(() => String)
  _id: Types.ObjectId;

  @Field(() => GraphQLISODateTime)
  _createdAt: Date;

  @Field(() => GraphQLISODateTime)
  _updatedAt: Date;

  @Prop({ type: ReactToSchema })
  @Field(() => ReactTo)
  to: ReactTo;

  @Prop({ type: ReactFromSchema })
  @Field(() => ReactFrom)
  from: ReactFrom;
}
export const ReactSchema = SchemaFactory.createForClass(React);

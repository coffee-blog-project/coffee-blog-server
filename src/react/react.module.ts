import { Module } from '@nestjs/common';
import { ReactResolver } from './react.resolver';

@Module({
  providers: [ReactResolver],
})
export class ReactModule {}

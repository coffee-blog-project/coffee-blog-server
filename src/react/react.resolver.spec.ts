import { Test, TestingModule } from '@nestjs/testing';
import { ReactResolver } from './react.resolver';

describe('ReactResolver', () => {
  let resolver: ReactResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReactResolver],
    }).compile();

    resolver = module.get<ReactResolver>(ReactResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});

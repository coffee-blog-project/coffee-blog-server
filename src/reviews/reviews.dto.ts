import { InputType, Field, PartialType } from '@nestjs/graphql';

export interface GetReviewArgs {
  writerId?: string 
  published?: boolean
}

export type GetReviewsArgs = GetReviewArgs

export type SearchReviewsArgs = GetReviewsArgs

@InputType()
export class CreateReviewArgs {
  @Field(() => String)
  productName: string;

  @Field(() => String)
  productImgUrl: string;

  @Field(() => String)
  content: string;

  @Field(() => Boolean)
  published: boolean;

  writerId: string;
}

@InputType()
export class UpdateReviewArgs extends PartialType(CreateReviewArgs) {}

import { ObjectType, Field, GraphQLISODateTime, ID } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@ObjectType()
export class Review extends Document {
  @Field(() => ID)
  _id: Types.ObjectId;

  @Field(() => GraphQLISODateTime)
  _createdAt: Date;

  @Field(() => GraphQLISODateTime)
  _updatedAt: Date;

  @Field(() => String)
  @Prop(() => String)
  productName: string;

  @Field(() => String, { nullable: true })
  @Prop(() => String)
  productImgUrl: string;

  @Field(() => String)
  @Prop(() => String)
  content: string;

  @Field(() => String)
  @Prop({ type: String })
  writerId: string;

  @Field(() => Boolean, { defaultValue: false })
  @Prop({ default: false })
  published: boolean;
}

export const ReviewSchema = SchemaFactory
  .createForClass(Review)
  .index({ productName: 'text', context: 'text', writerId: 'text' })

import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ReviewsService } from './reviews.service';
import { ReviewsResolver } from './reviews.resolver';
import { REVIEWS_SCHEMA_TOKEN } from './reviews.constant';
import { ReviewSchema } from './reviews.model';
import { UsersModule } from '../users/users.module';
import { CommentsModule } from '../comments/comments.module';
import { LikeModule } from '../like/like.module'
import { HistoryModule } from '../history/history.module'
import { SaveModule } from '../save/save.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: REVIEWS_SCHEMA_TOKEN,
        schema: ReviewSchema,
      },
    ]),
    forwardRef(() => UsersModule),
    forwardRef(() => CommentsModule),
    forwardRef(() => LikeModule),
    forwardRef(() => HistoryModule),
    forwardRef(() => SaveModule),
  ],
  providers: [ReviewsService, ReviewsResolver],
  exports: [ReviewsService],
})
export class ReviewsModule {}

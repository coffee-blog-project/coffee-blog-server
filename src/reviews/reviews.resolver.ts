import {
  Resolver,
  Mutation,
  Query,
  Args,
  ResolveField,
  Parent,
  Int,
  Context,
} from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { ReviewsService } from './reviews.service';
import { Review } from './reviews.model';
import { CreateReviewArgs, UpdateReviewArgs } from './reviews.dto';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.model';
import { CommentsService } from '../comments/comments.service';
import { Comment } from '../comments/comments.model';
import { AddressType } from '../react/react.model';
import { JwtAuthGuard, WeakJwtAuthGuard } from '../auth/auth.guard';
import { LikeService } from '../like/like.service'
import { HistoryService } from '../history/history.service'
import { Ip } from '../libs/decorators/ip';
import { SaveService } from '../save/save.service'

@Resolver(() => Review)
export class ReviewsResolver {
  constructor(
    private readonly reviewsService: ReviewsService,
    private readonly usersService: UsersService,
    private readonly commentsService: CommentsService,
    private readonly likeService: LikeService,
    private readonly historyService: HistoryService,
    private readonly saveService: SaveService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Review)
  createReview(
    @Args({
      name: 'review',
      type: () => CreateReviewArgs,
    })
    review: CreateReviewArgs,
    @Context('user') user: User,
  ) {
    return this.reviewsService.createReview({
      ...review,
      writerId: user._id as string,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Review)
  updateReview(
    @Args({
      name: '_id',
      type: () => String,
    })
    _id: string,
    @Args({
      name: 'review',
      type: () => UpdateReviewArgs,
    })
    review: UpdateReviewArgs,
    @Context('user') user: User,
  ) {
    return this.reviewsService.updateReview(_id, user._id as string, review);
  }

  @UseGuards(WeakJwtAuthGuard)
  @Query(() => Review)
  async review(
    @Args({
      name: '_id',
      type: () => String,
    })
    _id: string,
    @Context('user') user: User,
    @Ip() ip: string,
  ) {
    const review = await this.reviewsService.getReview(_id);
    if (review.published) {
      this.historyService.createHistory({
        to: {
          type: AddressType.review,
          _id,
        },
        from: {
          userId: user && user._id as string,
          ip,
        }
      })
    }

    return review
  }

  @Query(() => [Review])
  reviews() {
    return this.reviewsService.getReviews({
      published: true,
    });
  }

  @ResolveField(() => User)
  writer(@Parent() { writerId }: Review) {
    return this.usersService.getUserById(writerId);
  }

  @ResolveField(() => [Comment])
  comments(@Parent() { _id }: Review) {
    return this.commentsService.getComments({
      type: AddressType.review,
      _id: _id.toHexString(),
    });
  }

  @ResolveField(() => Boolean, { nullable: true })
  async liked(
    @Parent() { _id }: Review,
    @Context('user') user?: User,
  ) {
    const res = user ? !!await this.likeService.getLike({
      to: {
        type: AddressType.review,
        _id: _id.toHexString(),
      },
      from: {
        userId: user._id as string
      }
    }) : null
    return res
  }

  @ResolveField(() => Int, { defaultValue: 0 })
  async likes(
    @Parent() { _id }: Review,
  ) {
    return this.likeService.getLikesCount({
      type: AddressType.review,
      _id: _id.toHexString()
    })
  }

  @ResolveField(() => Int, { defaultValue: 0 })
  async views(
    @Parent() { _id }: Review,
  ) {
    return this.historyService.getViewsCount({
      type: AddressType.review,
      _id: _id.toHexString(),
    }) 
  }

  @ResolveField(() => Boolean, { nullable: true })
  async saved(
    @Parent() { _id }: Review,
    @Context('user') user?: User,
  ) {
    return user && !!await this.saveService.getSingleSave({
      to: {
        type: AddressType.review,
        _id: _id.toHexString(),
      },
      from: {
        userId: user._id as string
      }
    })
  }
}

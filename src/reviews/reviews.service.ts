import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Review } from './reviews.model';
import { REVIEWS_SCHEMA_TOKEN } from './reviews.constant';
import { CreateReviewArgs, UpdateReviewArgs, GetReviewArgs, GetReviewsArgs, SearchReviewsArgs } from './reviews.dto';
import * as _ from 'lodash'

@Injectable()
export class ReviewsService implements OnModuleInit {
  constructor(
    @InjectModel(REVIEWS_SCHEMA_TOKEN)
    private readonly reviewModel: Model<Review>,
  ) {}

  async onModuleInit() {
    await this.reviewModel.syncIndexes()
  }

  createReview(args: CreateReviewArgs) {
    return this.reviewModel.create(args);
  }

  updateReview(
    _id: Types.ObjectId | string,
    writerId: string,
    args: UpdateReviewArgs,
  ) {
    return this.reviewModel.findOneAndUpdate({ _id, writerId }, args);
  }

  getReviews(args?: GetReviewsArgs) {
    args = _.pickBy(args, _.identity)    
    return this.reviewModel.find(args).sort([['_createdAt', -1]]);
  }

  getReview(_id: Types.ObjectId | string, args?: GetReviewArgs) {
    return this.reviewModel.findOne({
      _id,
      ...args && {
        $or: [
          { writerId: args.writerId },
          { published: true },
        ]
      }
    });
  }

  search(query: string, args?: SearchReviewsArgs) {
    args = _.pickBy(args, _.identity)    
    return this.reviewModel.find({ 
      ...args,
      $text: { $search: query },
    }).exec()
  }
}

import { InputType, Field } from '@nestjs/graphql';
import { AddressType } from '../react/react.model';

export interface GetSaveArgs {
  to: SaveToArgs;
  from: SaveFromArgs;
}

@InputType()
export class SaveToArgs {
  @Field(() => AddressType)
  type: AddressType;

  @Field(() => String)
  _id: string;
}

export class SaveFromArgs {
  userId: string;
}

@InputType()
export class CreateSaveArgs {
  @Field(() => SaveToArgs)
  to: SaveToArgs;

  from: SaveFromArgs;
}

@InputType()
export class DeleteSaveArgs extends CreateSaveArgs {}

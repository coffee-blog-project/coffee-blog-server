import { ObjectType } from '@nestjs/graphql';
import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { React } from '../react/react.model';

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@ObjectType()
export class Save extends React {}

export const SaveSchema = SchemaFactory.createForClass(Save).index(
  { to: 1, from: 1 },
  { unique: true },
);

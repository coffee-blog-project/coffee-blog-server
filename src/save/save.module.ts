import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SaveService } from './save.service';
import { SaveResolver } from './save.resolver';
import { SaveSchema } from './save.model';
import { SAVE_SCHEMA_TOKEN } from './save.constant';
import { BlogsModule } from '../blogs/blogs.module';
import { ReviewsModule } from '../reviews/reviews.module'

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SAVE_SCHEMA_TOKEN,
        schema: SaveSchema,
      },
    ]),
    forwardRef(() => BlogsModule),
    forwardRef(() => ReviewsModule),
  ],
  providers: [SaveService, SaveResolver],
  exports: [SaveService],
})
export class SaveModule {}

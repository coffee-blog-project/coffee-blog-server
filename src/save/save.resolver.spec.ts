import { Test, TestingModule } from '@nestjs/testing';
import { SaveResolver } from './save.resolver';

describe('SaveResolver', () => {
  let resolver: SaveResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SaveResolver],
    }).compile();

    resolver = module.get<SaveResolver>(SaveResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});

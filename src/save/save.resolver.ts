import {
  Resolver,
  Mutation,
  Query,
  Args,
  Context,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/auth.guard';
import { SaveService } from './save.service';
import { Save } from './save.model';
import { CreateSaveArgs, DeleteSaveArgs } from './save.dto';
import { User } from '../users/users.model';
import { Types } from 'mongoose';
import { BlogsService } from '../blogs/blogs.service';
import { ReviewsService } from '../reviews/reviews.service'
import { AddressType } from '../react/react.model';
import { Post } from '../libs/models/post'

@UseGuards(JwtAuthGuard)
@Resolver(() => Save)
export class SaveResolver {
  constructor(
    private readonly saveService: SaveService,
    private readonly blogsService: BlogsService,
    private readonly reviewsService: ReviewsService,
  ) {}

  @Query(() => [Save])
  save(@Context('user') { _id }: User) {
    return this.saveService.getSave(_id as string);
  }

  @Mutation(() => Save)
  createSave(
    @Args({
      name: 'save',
      type: () => CreateSaveArgs,
    })
    like: CreateSaveArgs,
    @Context('user') { _id }: User,
  ) {
    return this.saveService.createSave({
      ...like,
      from: {
        userId: _id as string,
      },
    });
  }

  @Mutation(() => Boolean)
  async deleteSave(
    @Args({
      name: 'save',
      type: () => DeleteSaveArgs,
    })
    save: DeleteSaveArgs,
    @Context('user') { _id }: User,
  ) {
    const res = await this.saveService.deleteSave({
      ...save,
      from: {
        userId: _id as string,
      },
    });
    return res.n === 1;
  }

  @ResolveField(() => Post)
  source(@Parent() save: Save) {
    if (save.to.type === AddressType.blog)
      return this.blogsService.getBlog(save.to._id);
    if (save.to.type === AddressType.review)
      return this.reviewsService.getReview(save.to._id);
  }
}

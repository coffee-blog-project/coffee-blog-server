import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Save } from './save.model';
import { CreateSaveArgs, DeleteSaveArgs, GetSaveArgs } from './save.dto';
import { SAVE_SCHEMA_TOKEN } from './save.constant';

@Injectable()
export class SaveService {
  constructor(
    @InjectModel(SAVE_SCHEMA_TOKEN)
    private readonly saveModel: Model<Save>,
  ) {}

  createSave(args: CreateSaveArgs) {
    return this.saveModel.create(args);
  }

  deleteSave(args: DeleteSaveArgs) {
    return this.saveModel.deleteOne(args).exec();
  }

  getSingleSave(args: GetSaveArgs): Promise<Save> {
    return this.saveModel.findOne(args).exec();
  }

  getSave(userId: string): Promise<Save[]> {
    return this.saveModel
      .find({
        from: {
          userId,
        },
      })
      .exec();
  }
}

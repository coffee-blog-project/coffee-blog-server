import { Module } from '@nestjs/common';
import { SearchResolver } from './search.resolver';
import { BlogsModule } from '../blogs/blogs.module'
import { ReviewsModule } from '../reviews/reviews.module'

@Module({
  imports: [
    BlogsModule,
    ReviewsModule,
  ],
  providers: [SearchResolver]
})
export class SearchModule {}

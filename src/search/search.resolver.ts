import { Resolver, Query, Args } from '@nestjs/graphql';
import { BlogsService } from '../blogs/blogs.service'
import { ReviewsService } from '../reviews/reviews.service'
import { Post } from '../libs/models/post'

@Resolver()
export class SearchResolver {
  constructor(
    private readonly blogsService: BlogsService,
    private readonly reviewsService: ReviewsService,
  ) {}
  
  @Query(() => [Post])
  async search(
    @Args({
      name: 'query',
      type: () => String,
    })
    query: string
  ) {
    const blogs = await this.blogsService.search(query, {
      published: true
    })

    const reviews = await this.reviewsService.search(query, {
      published: true
    })

    return [...blogs, ...reviews]
  }
}

import { InputType, Field, PartialType, OmitType } from '@nestjs/graphql';
import { User } from './users.model';

@InputType()
export class CreateUserArgs
  implements Pick<User, 'name' | 'email' | 'password' | 'imageUrl' | 'facebookId'> {
  @Field(() => String, { nullable: true })
  imageUrl?: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  email?: string;

  @Field(() => String)
  password?: string;
  
  facebookId?: string

  googleId?: string
}

@InputType()
export class UpdateUserArgs extends PartialType(
  OmitType(CreateUserArgs, ['email', 'password']),
) {}

import { ObjectType, Field, GraphQLISODateTime, ID } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema({
  timestamps: {
    createdAt: '_createdAt',
    updatedAt: '_updatedAt',
  },
})
@ObjectType()
export class User extends Document {
  @Field(() => ID)
  _id: Types.ObjectId | string;

  @Field(() => GraphQLISODateTime)
  _createdAt: Date;

  @Field(() => GraphQLISODateTime)
  _updatedAt: Date;

  @Field(() => String)
  @Prop({ require: true })
  name: string;

  @Field(() => String, { nullable: true })
  @Prop({ required: false, unique: true, sparse: true })
  email?: string;

  @Prop({ required: false })
  password?: string;

  @Field(() => String, { nullable: true })
  @Prop({ required: false })
  imageUrl?: string;

  @Field(() => String, { nullable: true })
  @Prop({ required: false, unique: true, sparse: true })
  facebookId?: string

  @Field(() => String, { nullable: true })
  @Prop({ required: false, unique: true, sparse: true })
  googleId?: string
}

export const UserSchema = SchemaFactory.createForClass(User);

import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { USERS_SCHEMA_TOKEN } from './users.constant';
import { UserSchema } from './users.model';
import { AuthModule } from '../auth/auth.module'
import { BlogsModule } from '../blogs/blogs.module'
import { SaveModule } from '../save/save.module'
import { ReviewsModule } from '../reviews/reviews.module'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: USERS_SCHEMA_TOKEN, schema: UserSchema },
    ]),
    forwardRef(() => AuthModule),
    forwardRef(() => BlogsModule),
    forwardRef(() => SaveModule),
    forwardRef(() => ReviewsModule),
  ],
  providers: [UsersService, UsersResolver],
  exports: [UsersService],
})
export class UsersModule {}

import { UseGuards } from '@nestjs/common';
import {
  Resolver,
  Query,
  Mutation,
  Args,
  Context,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { UsersService } from './users.service';
import { User } from './users.model';
import { CreateUserArgs, UpdateUserArgs } from './users.dto';
import { AuthService } from '../auth/auth.service';
import { LocalAuthGuard, JwtAuthGuard } from '../auth/auth.guard';
import { BlogsService } from '../blogs/blogs.service';
import { Blog } from '../blogs/blogs.model';
import { Types } from 'mongoose';
import { SaveService } from '../save/save.service';
import { Save } from '../save/save.model'
import { ReviewsService } from '../reviews/reviews.service';
import { Review } from '../reviews/reviews.model'

@Resolver(() => User)
export class UsersResolver {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly blogsService: BlogsService,
    private readonly reviewsService: ReviewsService,
    private readonly saveService: SaveService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Query(() => User)
  me(@Context('user') { _id }: User) {
    return this.usersService.getUserById(_id);
  }

  @Query(() => [User])
  users() {
    return this.usersService.getUsers();
  }

  @Mutation(() => User)
  createUser(
    @Args({
      name: 'user',
      type: () => CreateUserArgs,
    })
    user: CreateUserArgs,
  ) {
    return this.usersService.createUser(user);
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => User)
  updateUser(
    @Args({
      name: 'user',
      type: () => UpdateUserArgs,
    })
    user: UpdateUserArgs,
    @Context('user') { _id }: User,
  ) {
    return this.usersService.updateUser(_id, user);
  }

  @UseGuards(LocalAuthGuard)
  @Mutation(() => String)
  login(
    @Args({
      name: 'email',
      type: () => String,
    })
    email: string,
    @Args({
      name: 'password',
      type: () => String,
    })
    password: string,
    @Context('user') user: User,
  ) {
    return this.authService.generateJwt(user);
  }

  @ResolveField(() => [Blog], { nullable: true })
  blogs(@Parent() { _id }: User) {
    return this.blogsService.getBlogs({
      writerId: (_id as Types.ObjectId).toHexString(),
    });
  }

  @ResolveField(() => [Review], { nullable: true })
  reviews(@Parent() { _id }: User) {
    return this.reviewsService.getReviews({
      writerId: (_id as Types.ObjectId).toHexString(),
    })
  }

  @UseGuards(JwtAuthGuard)
  @ResolveField(() => [Save])
  save(@Context('user') { _id }: User) {
    return this.saveService.getSave(_id as string);
  }
}

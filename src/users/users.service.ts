import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { USERS_SCHEMA_TOKEN } from './users.constant';
import { User } from './users.model';
import { CreateUserArgs, UpdateUserArgs } from './users.dto';
import { hashSync } from 'bcrypt';

@Injectable()
export class UsersService implements OnModuleInit {
  constructor(
    @InjectModel(USERS_SCHEMA_TOKEN)
    private readonly userModel: Model<User>,
  ) {}

  onModuleInit() {
    this.userModel.syncIndexes();
  }

  createUser(args: CreateUserArgs) {
    if (args.password) {
      args.password = hashSync(args.password, 10);
    }
    return this.userModel.create(args);
  }

  updateUser(_id: Types.ObjectId | string, args: UpdateUserArgs) {
    return this.userModel.findByIdAndUpdate(_id, args);
  }

  getUsers() {
    return this.userModel.find();
  }

  getUserById(_id: Types.ObjectId | string) {
    return this.userModel.findById(_id);
  }

  getUserByEmail(email: string): Promise<User> {
    return this.userModel.findOne({ email }).exec();
  }

  getUserByFacebookId(facebookId: string) {
    return this.userModel.findOne({ facebookId }).exec()
  }

  getUserByGoogleId(googleId: string) {
    return this.userModel.findOne({ googleId }).exec()
  }
}
